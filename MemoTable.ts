import { MemoData, MemoRow } from './MemoRow'
import { Row } from './Row'
import { Table } from './Table'

export class MemoTable extends Table<MemoData> {
  getTableName(): string {
    return 'memo'
  }

  async getById(id: number): Promise<MemoRow> {
    let row = await super.getById(id)
    return row as MemoRow
  }

  createRow(id: number): MemoRow {
    return new MemoRow(this, id)
  }
}
