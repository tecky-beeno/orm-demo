import { NotFoundError } from './NotFoundError'
import { Row, RowData } from './Row'
import { UserTable } from './UserTable'

export interface UserData extends RowData {
  username: string
}

export class UserRow extends Row<UserData, UserTable> {
  async rename(username: string) {
    if (username.length < 3) {
      throw new Error('Username should contains at least 3 characters')
    }
    if (username.length > 32) {
      throw new Error('Username cannot be longer than 32 characters')
    }
    try {
      let other = await this.getTable().getByUsername(username)
      if (other.getId() === this.getId()) {
        return
      }
      throw new Error('The username is already occupied by another user')
    } catch (error) {
      if (!(error instanceof NotFoundError)) {
        throw error
      }
      // the new name is not used
    }
    await this.save({ username })
  }
}
