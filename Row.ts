import { Table } from './Table'

export interface RowData {
  id: number
}

export abstract class Row<Data extends RowData, T extends Table<Data>> {
  constructor(private table: T, private id: number) {}

  protected getTable(): T {
    return this.table
  }

  public getId() {
    return this.id
  }

  protected async save(data: Partial<Data>): Promise<void> {
    await this.table.update(this.id, data)
  }
}
