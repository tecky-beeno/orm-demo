import { RowData } from './Row'
import { Table } from './Table'

export class NotFoundError extends Error {
  constructor(table: Table<RowData>) {
    super(`${table.getTableName()} not found`)
  }
}
