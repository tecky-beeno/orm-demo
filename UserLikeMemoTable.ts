import { Table } from './Table'
import { UserLikeMemoData, UserLikeMemoRow } from './UserLikeMemoRow'

export class UserLikeMemoTable extends Table<UserLikeMemoData> {
  getTableName(): string {
    return 'user_like_memo'
  }

  createRow(id: number): UserLikeMemoRow {
    return new UserLikeMemoRow(this, id)
  }

  async searchByFilter(
    filter: Partial<UserLikeMemoData>,
  ): Promise<UserLikeMemoRow[]> {
    let rows = await super.searchByFilter(filter)
    return rows as UserLikeMemoRow[]
  }
}
