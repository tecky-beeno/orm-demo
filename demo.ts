import Knex from 'knex'
import { MemoTable } from './MemoTable'
import { UserTable } from './UserTable'

let knex = Knex({})

let userTable = new UserTable(knex)
let memoTable = new MemoTable(knex)

async function main() {
  let alice = await userTable.getById(1)

  let user = await userTable.getByUsername('alice')
  await user.rename('alice123')

  let memo = await memoTable.getById(2)
  await memo.like(user)
}
main()
