import { Table } from './Table'
import { UserData, UserRow } from './UserRow'

export class UserTable extends Table<UserData> {
  getTableName(): string {
    return 'user'
  }

  createRow(id: number): UserRow {
    return new UserRow(this, id)
  }

  async getByUsername(username: string): Promise<UserRow> {
    return (await this.findByFilter({ username })) as UserRow
  }
}
