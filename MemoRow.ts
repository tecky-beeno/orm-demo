import { MemoTable } from './MemoTable'
import { Row } from './Row'
import { UserLikeMemoTable } from './UserLikeMemoTable'
import { UserRow } from './UserRow'
import { UserTable } from './UserTable'

export interface MemoData {
  id: number
  content: string
}

export class MemoRow extends Row<MemoData, MemoTable> {
  constructor(
    private userLikeMemoTable: UserLikeMemoTable,
    private userTable: UserTable,
    table: MemoTable,
    id: number,
  ) {
    super(table, id)
  }
  async like(user: UserRow) {
    await this.userLikeMemoTable.create({
      memo_id: this.getId(),
      user_id: user.getId(),
    })
  }
  async getLikers(): Promise<UserRow[]> {
    let rows = await this.userLikeMemoTable.searchByFilter({
      memo_id: this.getId(),
    })
    let userIds = await Promise.all(rows.map(row => row.getUserId()))
    let users = userIds.map(userId => this.userTable.createRow(userId))
    return users
  }
}
