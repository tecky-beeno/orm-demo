import { Knex } from 'knex'
import { NotFoundError } from './NotFoundError'
import { Row, RowData } from './Row'

export abstract class Table<Data extends RowData> {
  constructor(private knex: Knex) {}

  abstract getTableName(): string

  abstract createRow(id: number): Row<Data, Table<Data>>

  async getById(id: number) {
    let filter: Partial<Data> = {}
    filter.id = id
    return this.findByFilter(filter)
  }

  async findByFilter(filter: Partial<Data>): Promise<Row<Data, Table<Data>>> {
    let rowData: Data | undefined = await this.knex
      .select('id')
      .from(this.getTableName())
      .where(filter)
      .first()
    if (!rowData) {
      throw new NotFoundError(this)
    }
    return this.createRow(rowData.id)
  }

  async getProperty<K extends keyof Data>(
    id: number,
    field: K,
  ): Promise<Data[K]> {
    let rowData: Data | undefined = await this.knex
      .select(field)
      .from(this.getTableName())
      .where('id', id)
      .first()
    if (!rowData) {
      throw new NotFoundError(this)
    }
    return rowData[field]
  }

  async searchByFilter(
    filter: Partial<Data>,
  ): Promise<Array<Row<Data, Table<Data>>>> {
    let rows: Data[] = await this.knex
      .select('id')
      .from(this.getTableName())
      .where(filter)
    return rows.map(row => this.createRow(row.id))
  }

  async update(id: number, data: Partial<Data>): Promise<void> {
    await this.knex.from(this.getTableName()).update(data).where('id', id)
  }

  async create(data: Partial<Data>) {
    let rows = await this.knex
      .insert(data)
      .into(this.getTableName())
      .returning('id')
    let id = rows[0].id as number
    return this.createRow(id)
  }
}
