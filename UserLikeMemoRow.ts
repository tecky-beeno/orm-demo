import { Row, RowData } from './Row'
import { UserLikeMemoTable } from './UserLikeMemoTable'

export interface UserLikeMemoData extends RowData {
  memo_id: number
  user_id: number
}
export class UserLikeMemoRow extends Row<UserLikeMemoData, UserLikeMemoTable> {
  async getUserId() {
    return this.getTable().getProperty(this.getId(), 'user_id')
  }
}
